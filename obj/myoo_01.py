class Student:  # 类名一般首字母大写,多个单词采用驼峰原则

    def __init__(self, name, score):    # self必须位于第一个参数
        self.name = name
        self.score = score

    def say_score(self):        # self必须位于第一个参数
        print(f"{self.name}的分数是: {self.score}")


s1 = Student("wlk", 180)
s1.say_score()