"""
输入一个分数,分数在0-100之间......
"""
# 选择结构的嵌套
score = int(input("请输入一个分数: "))
grade = ""
if score > 100 or score < 0:
    print("请重新输入一个一个0-100的分数")
else:
    if score >= 90:
        print("A")
        grade = "A"
    elif score >= 80:
        print("B")
        grade = "B"
    elif score >= 70:
        print("C")
        grade = "C"
    elif score >= 60:
        print("D")
        grade = "D"
    else:
        print("E")
        grade = "E"
    print("分数为{0},等级为{1}".format(score, grade))

print("=====================================================")
degree = "ABCDE"
num = 0
if score > 100 or score < 0:
    print("请重新输入一个一个0-100的分数")
else:
    num = score // 10
    if num < 6:
        num = 5

    print(degree[9 - num])
