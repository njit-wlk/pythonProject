s=input("请输入一个数字:")
if int(s)<10:
    print("s是小于10的数字")
else:
    print("s是大于10的数字")

#三元条件运算符,简洁易读
#语法: 条件为真时的值 if (条件表达式) else 条件为假时的值
num = input("请再输入一个数字:")
print(num if int(num)<10 else "数字太大")