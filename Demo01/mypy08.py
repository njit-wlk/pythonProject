# break语句
while True:
    a = input("请输入一个字符(输入Q或者q时退出): ")
    if a == "q" or a == "Q":
        print("循环结束,退出")
        break
    else:
        print(a)

# continue语句
"""
输入员工的薪资,若薪资小于0则重新输入.最后打印出录入员工的数量和薪资明细,以及平均薪资
"""
empNum = 0
salarySum = 0
salarys = []
while True:
    s = input("请输入员工的薪资(输入Q或者q时退出): ")

    if s.upper() == 'Q':
        print("录入完成,退出")
        break
    if float(s) < 0:
        continue
    empNum += 1
    salarys.append(float(s))
    salarySum += float(s)

print("员工数{0}".format(empNum))
print("录入薪资: ", salarys)
print("平均薪资{0}".format(salarySum / empNum))

# else语句
# while.for循环可以附带一个else语句(可选).如果for,while语句没有被break语句结束,则会执行else语句,否则不执行.语法格式如下:
"""
while 条件表达式
    循环体
else:
    语句块
    
或者:
for 变量 in 可迭代对象
    循环体
else:
语句块

"""