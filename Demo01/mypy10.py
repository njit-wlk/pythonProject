"""
使用zip()并行迭代
我们可以使用zip()函数对多个序列进行并行迭代,zip()函数在最短序列"用完"时就会停止
"""
for i in [1, 2, 3]:
    print(i)

names = ("坤泰", "张三", "李四", "王五")
ages = (18, 19, 20, 21)
jobs = ("老师", "程序员", "公务员")
for name, age, job in zip(names, ages, jobs):
    print("{0}---{1}---{2}".format(name, age, job))

for i in range(3):
    print("{0}---{1}---{2}".format(names[i],ages[i],jobs[i]))
