# 推导式创建序列, 典型python风格

# 列表推导式
y = [x * 2 for x in range(1, 50) if x % 5 == 0]
print(y)

y = []
for x in range(1, 50):
    if x % 5 == 0:
        y.append(x * 2)
print(y)

cells = [(row, col) for row in range(1, 5) for col in range(1, 5)]
print(cells)

print("================================================================")

# 字典推导式
# 格式: {key_expression : value_expression for 表达式 in 可迭代对象}
#  类似于列表推导式,字典推导也可以增加if条件判断,多个for循环
my_text = "i love you, i love wlk, i love sxt"
char_count = {c: my_text.count(c) for c in my_text}
print(char_count)

print("===============================================================")
# 集合推导式
b = {x for x in range(1, 100) if x % 9 == 0}
print(b)

print("===============================================================")
# 生成器推导式(生成元组)
# 一个生成器只能运行一次,第一次迭代可以得到数据,第二次迭代数据就没有了
gnt = (x for x in range(4))
# print(tuple(gnt))
# print(tuple(gnt))
for x in gnt:  # 生成器是可迭代对象
    print(x, end=' ')
    print(x, end=' ')
