b = []
if not b:
    print("空列表是false")

c = "True"    #非空字符串也是: True
if c:
    print("c")

d = 10         #d = 0
if d:
    print("d")

if 0 < d < 100:
    print("0<d<100")


if True:
    print("TRUE")

#条件判断,不能出现赋值语句