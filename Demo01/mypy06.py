"""
for循环通常用于可迭代对象的遍历.语法如下:
for 变量 in 可迭代对象
    循环语句
"""
for x in (20, 30, 40):
    print(x * 3)

# 遍历字符串中的字符
for x in "abide001":
    print(x)

# 遍历字典
d = {"name": "坤泰", "age": 20, "job": "student"}
for x in d:
    print(x)
for x in d.keys():
    print(x)
for x in d.values():
    print(x)
for x in d.items():
    print(x)

print("=========================")

for x in range(5):
    print(x)

# 计算1~100之间的奇数累加和和偶数累加和
sum_all = 0     # 总和
sum_even = 0    # 1~100偶数的累加和
sum_odd = 0     # 1~100奇数的累加和
for num in range(101):
    sum_all += num
    if num%2 == 0:
        sum_even += num
    else:
        sum_odd += num
    num += 1    # 迭代,改变条件表达式
print("1~100所有数的累加和",sum_all)
print("1~100所有偶数的累加和",sum_even)
print("1~100所有奇数的累加和",sum_odd)
print("1~100累加总和{},偶数和{},奇数和{}".format(sum_all,sum_even,sum_odd))