# 画同心圆
import turtle

t = turtle.Pen()

my_colors = ("red", "green", "yellow", "pink",)
t.width(4)
t.speed(10)

for i in range(50):
    t.penup()
    t.goto(0, -i * 50)
    t.pendown()
    t.color(my_colors[i % 4])
    t.circle(0 + i * 50)

turtle.done()  # 程序执行完,窗口不关闭
