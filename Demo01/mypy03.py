"""
输入一个学生的成绩,将其简单描述为:不及格(小于60),
及格(60~79),良好(80~89),优秀(90~100)
"""
score = int(input("请输入分数:"))
grade = ""
if score<60:
    grade = "不及格"
elif score<80:
    grade = "及格"
elif score<90:
    grade = "良好"
else:
    grade = "优秀"

print("分数是{0},等级是{1}".format(score,grade))