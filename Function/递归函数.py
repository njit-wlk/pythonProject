"""
递归函数
1.终止条件
    表示递归什么时候结束.一般用于返回值,不再调用自己
2.递归步骤
    把第 n 布与 n-1 相关联
"""


def test01(n):
    print("test01: ", n)
    if n == 0:
        print("over")
    else:
        test01(n - 1)
    print("test01***", n)


def test02():
    print("test02")


test01(4)

"""
结果如下:
test01:  4
test01:  3
test01:  2
test01:  1
test01:  0
over
test01*** 0
test01*** 1
test01*** 2
test01*** 3
test01*** 4
"""