a = 3  # 全局变量


def test01():
    b = 4
    print(b * 4)

    global a  # 如果在函数内改变全局变量的值,使用global关键字声明
    a = 300
    print(a)

    print(locals())  # 打印输出的局部变量
    print("#" * 20)
    print(globals())  # 打印输出全局变量


test01()

print(a)
