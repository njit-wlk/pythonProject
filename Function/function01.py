# 测试函数的定义和调用
def test01():
    print("*" * 10)
    print("@" * 10)


print(id(test01))
print(type(test01))
test01()


def sayHello():
    print("Hello,World!")


sayHello()  # call the function
