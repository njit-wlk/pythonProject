"""
DocStrings
Python有个很奇妙的特性,称为 文档字符串 ,它通常被简称为 docstrings
DocStrings是一个重要的工具,由于它帮助你的程序文档更加简单易懂,我们尽量使用它.我们甚至可以在程序运行时,从函数恢复文档字符串!
"""


def printMax(x, y):
    """
    Print the maximum of two numbers.The two values must be integers.
    :param x: int
    :param y: int
    :return:
    """

    x = int(x)  # convert to integers,if possible
    y = int(y)

    if x > y:
        print(x, "is maximum")
    else:
        print(y, "is maximum")


printMax(3, 5)
print(printMax.__doc__)
help(printMax.__doc__)
