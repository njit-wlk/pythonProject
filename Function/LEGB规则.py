"""
LEGB:

Local       函数或者类的方法内部
Enclosed    嵌套函数(一个函数包裹另一个函数,闭包)
Global      模块中的全局变量
Built in    Python为自己保留的特殊名称
"""


#str = "global str"

def outer():

    #str = "outer"

    def inner():
        #str = "inner"
        print(str)

    inner()


outer()
