# 使用递归函数,计算阶乘
# 5! = 5 * 4 * 3 * 2 * 1


def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n - 1)


result = factorial(5)
print(result)
