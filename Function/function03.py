# 局部变量和全局变量的测试
# 局部变量的查询和访问速度比全局变量快,优先考虑使用,尤其是在循环的时候在特别强调效率的地方或者循环次数比较多的地方,可以将全局变量转化为局部变量提高运行速度

import math
import time


def test01():
    start = time.time()
    for i in range(1000000):
        math.sqrt(30)
    end = time.time()
    print("耗时为{}".format((end - start)))


def test02():
    b = math.sqrt
    start = time.time()
    for i in range(1000000):
        b(30)
    end = time.time()
    print("耗时为{}".format((end - start)))


test01()
test02()
