# 嵌套函数(内部函数): 在函数内部定义的函数
def outer():
    print("outer is running")

    def inner():
        print("inner is running")

    inner()


outer()
"""
一般在什么时候使用嵌套函数:
1. 封装 - 数据隐藏
    外部无法访问"嵌套函数"
2. 贯彻DRY(Don't Repeat Yourself)原则
    嵌套函数,可以让我们在函数内部避免使用重复的代码
3. 闭包
    后面详解 
"""


def printName(isChinese, name, familyName):
    def inner_print(a, b):
        print(f"{a} {b}")

    if isChinese:
        inner_print(familyName, name)
    else:
        inner_print(name, familyName)


printName(True, "linkun", "Wu")
printName(False, "Peter", "Parker")
