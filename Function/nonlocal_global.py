# nonlocal关键字
# nonlocal    用来声明外层的局部变量
# global      用来声明全局变量

def outer():
    b = 10

    def inner():
        nonlocal b   # 声明外部函数的局部变量
        print("inner b:", b)
        b = 20

    inner()
    print("outer b:", b)


outer()
