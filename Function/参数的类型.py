# 参数的几种类型 :1.位置参数      2.默认值参数         3.命名参数      4.可变参数

def test01(a, b, c, d):
    print("{0}---{1}---{2}---{3}".format(a, b, c, d))


test01(10, 20, 30, 40)  # 位置参数

test01(d=20, c=10, b=55, a=66)  # 命名参数


def test02(a, b, c=10, d=15):
    print("{0}---{1}---{2}---{3}".format(a, b, c, d))


test02(2, 3)
test02(2, 3, 4)
