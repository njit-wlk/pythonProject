# 形参和实参( param )
def printMax(a, b):
    if a > b:
        print(a, "is maximum")
    else:
        print(b, "is maximum")


printMax(3, 4)  # directly give literal values
printMax(200, 300)

x = 5
y = 7
printMax(x, y)  # give variables as arguments
